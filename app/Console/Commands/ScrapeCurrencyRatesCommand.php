<?php
namespace App\Console\Commands;
use App\Services\CurrencyRateService;
use Illuminate\Console\Command;

class ScrapeCurrencyRatesCommand extends Command
{
    protected $signature = 'scrape:currency-rates';
    protected $description = 'Scrape currency rates from kursdollar.org and save to database';

    public function handle(CurrencyRateService $service)
    {
        $service->scrapeAndSave();
        $this->info('Currency rates scraped and saved successfully.');
    }
}
