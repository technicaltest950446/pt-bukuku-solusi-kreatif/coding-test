<?php

namespace App\Http\Controllers;

use App\Services\CurrencyRateService;
use App\Repositories\CurrencyRateRepository;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Storage; // Import the Storage facade
use Illuminate\Support\Facades\File;

class CurrencyRateController extends Controller
{
    protected $service;
    protected $repository;

    public function __construct(CurrencyRateService $service, CurrencyRateRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function scrapeAndSave()
    {
        return response()->json([
          'message' => 'Scraping and saving successful.',
          'data' => $this->service->scrapeAndSave(),
        ]);
    }

    public function deleteAll()
    {
        $this->repository->deleteAll();
        // Storage::delete(\Storage::files('rates-'));
        $files = File::glob(storage_path("rate"));
        
        // Dispatch the job to delete currency rates
        DeleteCurrencyRates::dispatch();

        // foreach ($files as $file) {
        //     File::delete($file);
        // }
        return response()->json(['message' => 'Proses berhasil ditambahkan pada Job.']);
    }
}
