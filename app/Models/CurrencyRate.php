<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencyRate extends Model
{
    use HasFactory;
    protected $fillable = ['currency', 'buy', 'sell', 'average', 'word_rate', 'created_at'];
}
