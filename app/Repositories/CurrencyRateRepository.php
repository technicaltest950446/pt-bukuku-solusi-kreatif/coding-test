<?php
namespace App\Repositories;

use App\Models\CurrencyRate;

class CurrencyRateRepository
{
    public function all()
    {
        return CurrencyRate::all();
    }

    public function deleteAll()
    {
        CurrencyRate::truncate();
    }

    public function create(array $data)
    {
        return CurrencyRate::create($data);
    }
}