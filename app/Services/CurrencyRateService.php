<?php
namespace App\Services;

use Goutte\Client;
use App\Repositories\CurrencyRateRepository;
use Illuminate\Support\Facades\Storage; // Import the Storage facade

class CurrencyRateService
{
    protected $repository;

    public function __construct(CurrencyRateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function scrapeAndSave()
    {
        $client = new Client();
        $crawler = $client->request('GET', 'https://kursdollar.org');

        // Scraping logic here
        $tableCrawler = $crawler->filter('div.col-md-12 table.in_table');

        // Loop through each row in the table
        $tableCrawler->filter('tr')->each(function ($rowCrawler) use (&$currencyRates) {
          // Extract data from each row
          $currency = $rowCrawler->filter('.currency')->text();
          $buyRate = $rowCrawler->filter('.kurs-beli')->text();

          // Add the extracted data to the result array
          $currencyRates[] = [
              'currency' => $currency,
              'buy' => $buyRate,
              // Add other rate details
          ];
        });
        $data=[
          "meta"=>[
            "date"=> "29-04-2022",
            "day"=> "Friday",
            "indonesia"=> "Bank Indonesia - 29 Apr, 12:00",
            "word"=> "Spot Dunia 29 Apr, 13:00"
          ],
          "rates"=>$currencyRates,
        ];
        // Save data to the repository
        // $this->repository->create($data['meta']);
        // Save data to a JSON file
        $this->saveToJsonFile($data);
        // $crawler->dd();
        // dd( $crawler->html());
        echo $tableCrawler->html();

      return $data;
    }
    protected function saveToJsonFile($data)
    {
        $filePath = 'storage/rates-' . now()->format('d-m-Y--H-i-s') . '.json'; // Set the file path

        // Save data to JSON file
        Storage::put($filePath, json_encode($data, JSON_PRETTY_PRINT));

        // You can store the file path in the database if needed
        // For example, $this->repository->storeFilePath($filePath);
        // $this->info('Currency rates have been scraped and updated.');
    }
}
